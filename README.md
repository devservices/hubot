# Hubot #

### What is this repository for? ###

* This repository contains an Ansible playbook to deploy Hubot to a remote server and integrate it with DD Slack.
* For full documentation, see the [Hubot Wiki](https://devservices.jira.com/wiki/display/hubot).

### How do I get set up? ###

* Hubot requires a remote server, I recommend a free tier AWS T2-Micro EC2 instance.
* Your local machine running the Hubot deployment requires Python 2.6 or 2.7 and Ansible.
* Configure by editing hubot/inv/server and hubot/inv/group_vars/all.
* Hubot is deployed with the command 
```
#!bash

ansible-playbook hubot.yml -i inv/server --private-key /<path_to_private_key>
```

### Contribution guidelines ###

* If you would like to contribute to the ddbot, the DD wide bot, then please pull this repo, make any changes, and deploy.
* If you would like to create a project specific Hubot instance, then please clone this repo, configure to work with your project specific tools, and deploy.

### Who do I talk to? ###

* Sam Warley
* swarley@deloitte.co.uk